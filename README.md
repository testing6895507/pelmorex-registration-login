## Registration and login APP developed using the MERN stack,

# Requirements

To build and run this application, you will need to have the following:
 - *Node.js (version 12 or higher*
 - *MongoDB (version 4.2 or higher)*

### *Registration:*

 - Name
 - Email
 - Password
 - Password confirmation

#####   *Business rules for the registration:* 
 - All fields are required
 - Password must be greater than 12 chars with at least
 - one special char
 - one number
 - one uppercase letter

### *Validate email:*
 - Do not accept an email that already exists in the database
###    *Login:*
 - Email
 - Password
#####   *Business rules for the registration:* 
 - If email and pass don't match, show message "Incorrect email or
   password"
 - If email and pass match, redirect user to a page that contains the
   following text:

> ""If you want to see the sunshine, you have to weather the storm.""

# Running the Application
**To run the application locally, follow the steps below:**

 1. Clone the repository from GitLab using the command:  `git clone https://gitlab.com/testing6895507/pelmorex-registration-login.git`
 
 2. Navigate to the pelmorex-registration-login folder and open the client and server 
                                       subfolders in  separate terminal windows.
                                       
 3. In each terminal window, run the command `npm install` to install the necessary dependencies.
 
 5. To start the server, run the command `npm start` in the server folder. 
    This will launch the backend on localhost:8080 using Nodemon.
    
 6. To start the client, run the command npm start in the client folder. 
    This will launch the frontend on localhost:3000.*

> ### Note
> 
> Both the server and client folders contain additional README files
> with specific  information about each component of the application.
