const { check } = require("express-validator");

const validateFields = (fields) => {
  const validators = [];

  if (!Array.isArray(fields)) {
    throw new Error("Fields must be an array");
  }

  fields.forEach((field) => {
    switch (field) {
      case "name":
        validators.push(
          check(field).notEmpty().withMessage("Name is required")
        );
        break;
      case "email":
        validators.push(
          check(field).isEmail().withMessage("Email must be valid")
        );
        break;
      case "password":
        validators.push(
          check(field)
            .isLength({ min: 12 })
            .withMessage("Password must be at least 12 characters long")
            .matches(/[!@#$%^&*(),.?":{}|<>]/)
            .withMessage("Password must contain at least one special character")
            .matches(/[0-9]/)
            .withMessage("Password must contain at least one number")
            .matches(/[A-Z]/)
            .withMessage("Password must contain at least one uppercase letter")
        );
        break;
      case "passwordConfirm":
        validators.push(
          check(field).custom((value, { req }) => {
            if (value !== req.body.password) {
              throw new Error("Passwords do not match");
            }
            return true;
          })
        );
        break;
      default:
        break;
    }
  });

  return validators;
};

module.exports = { validateFields };
