const Router = require("express");
const router = new Router();
const controller = require("../controllers/authController");
const { validateFields } = require("../midlware/validateFields");
const validateAuth = require("../midlware/validateAuth");
const roles = require("../midlware/roles");

router.post(
  "/signup",
  validateFields(["name", "email", "password", "passwordConfirm"]),
  controller.registration
);
router.post("/login", validateFields(["email", "password"]), controller.login);
router.post("/logout");

router.get("/users", controller.getUser);
// router.get('/users', roles(["ADMIN"]), controller.getUser)

module.exports = router;
