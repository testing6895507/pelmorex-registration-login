const User = require("../model/User");
const Role = require("../model/Role");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
require("dotenv").config();
const geneateAccesToken = (id, roles) => {
  const payload = {
    id,
    roles,
  };
  return jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: "8h" });
};
class authController {
  //----registration----
  async registration(req, res) {
    console.log(req.body)
   
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: "Validation Error wrong Email or pass", errors });
      }
      const { name, email, password, passwordConfirm } = req.body;
      const candidate = await User.findOne({ email, password });
      if (candidate) {
        return res.status(400).json({
          message: "We're sorry, but the email and password you entered are already in use. Please try logging."
        });
      }
      const salt = bcrypt.genSaltSync(2);
      const hashPassword = bcrypt.hashSync(password, salt);
      const userRole = await Role.findOne({ value: "USER" });

      const user = new User({
        name,
        email,
        password: hashPassword,
        roles: [userRole],
      });
      await user.save();
      const token = geneateAccesToken(user._id, user.roles);
      return res.status(201).json({ message: "Account successfully created", token });
    } catch (e) {
      console.log(e);
      res.status(500).json({ message: "Registraion error" });
    }
  }
  
  //----login------
  async login(req, res) {
    
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res
          .status(400)
          .json({
            message: `Incorrect email or password`,
          });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({ message: `Password is incorrect` });
      }
      const token = geneateAccesToken(user._id, user.roles);
      res.status(200).json({ message: '"Your login was successful. You will be redirected to the success page shortly. Thank you ', token  });
    
    } catch (e) {
      console.log(e);
      return res.status(500).json({ message: "Internal server error" });
      
    }
  }

  // for ADMIN ony. this is for testing 
  async getUser(req, res) {
    console.log(req.body)
    try {
      const users = await User.find()
      res.json(users)
    } catch (e) {
      console.log(e);
      return res.status(500).json({ message: "Internal server error" });
    }
  }
}

module.exports = new authController();
