const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
mongoose.set("strictQuery", false);
const cors = require("cors");
const authRouter = require("./router/autRouter");

const app = express();
app.use(cors());
app.use(express.json());
app.use("", authRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URL);
    app.listen(process.env.PORT || 3000, () =>
      console.log(`server started on port ${process.env.PORT}`)
    );
  } catch (e) {
    console.log(e);
  }
};

start();
