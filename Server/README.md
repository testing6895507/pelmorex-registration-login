## Registration and login APP _Backend

This project is a Node.js server application that manages user accounts and permissions. Its purpose is to provide a simple and secure way to create and manage user accounts with different levels of access.

### *# Installation:*

 - 1.  Clone the repository to your local machine.
 - 2.  Open a terminal window and navigate to the project directory.
 - 3.  Install the required Node.js packages by running `npm install`.
 - 4.  Remove the `.dev` extension from the `.env.dev` 
 - 5.  start server   `npm start` 
 - 
 To test the server, you can install the REST Client extension in 
 VS Code. I have already created requests in the `request.rest.dev`  file for sending GET and POST requests directly in VS Code.

###   *# Routes:* 

> ##### POST http://localhost:3010/signup
> ##### POST http://localhost:3010/login
> ##### GET http://localhost:3010/users

To register a new user, send a `POST` request to the `/signup` endpoint with the following parameters:
-   `name`: the name of the user
-   `email`: the user's email address
-   `password`: the user's password

    - All fields are required - Password must be greater than 12 chars with at least - one special char - one number - one uppercase letter

 Example request:
 
 ##### POST http://localhost:3010/signup 
 ##### Content-Type: application/json 
 ##### { 
 ##### "name": "John Doe", 
 ##### "email": "johndoe@example.com", 
 ##### "password": "secretpassword"
#####  };

### List Users

To list all users, send a `GET` request to the `/users` endpoint. This route requires authentication and admin access. Include the user's email and password, as well as the authentication token in the request headers.
Example request:

##### GET http://localhost:3010/users 
##### Content-Type: application/json Authorization: 
##### Bearer   tokwn
##### Email: johndoe@example.com 
##### Password: secretpassword
