const { Schema, model } = require("mongoose");

const User = new Schema({
  name: { type: String, require: true },
  email: { type: String, unique: true, require: true },
  password: { type: String, unique: true, require: true },
  roles: [{ type: String, ref: "Role" }],
});

module.exports = model("User", User);
