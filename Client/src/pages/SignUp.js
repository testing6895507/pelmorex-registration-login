import { useState } from "react";
import PageContent from "../components/PageContent";
import styles from "./Sign.module.css";
import useInput from "../hooks/use-input";
import {
  checkName,
  checkEmail,
  checkPassword,
  checkMatch,
} from "../hooks/validator";
import { Link } from "react-router-dom";
import { FetchSignUp } from "../fetch/FetchSignUp";

// input validation
const isName = (value) => checkName(value);
const isEmail = (value) => checkEmail(value);
const isValidPass = (value) => checkPassword(value);
const isValidConf_pass = (value) => checkMatch(value);

function SignUpPage() {
  const [error, setError] = useState(null);
  console.log(error);
  const [serverRes, setServerRes] = useState(null);

  const {
    value: nameValue,
    isValid: nameIsValid,
    hasError: nameHasError,
    valueChangeHandler: nameChangeHandler,
    inputBlurHandler: nameBlurHandler,
    reset: resetName,
  } = useInput(isName);
  const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);
  const {
    value: passValue,
    isValid: passIsValid,
    hasError: passHasError,
    valueChangeHandler: passChangeHandler,
    inputBlurHandler: passBlurHandler,
    reset: resetPass,
  } = useInput(isValidPass);
  const {
    value: conf_passValue,
    isValid: conf_passIsValid,
    hasError: conf_passHasError,
    valueChangeHandler: conf_passChangeHandler,
    inputBlurHandler: conf_passBlurHandler,
    reset: resetComf_pass,
  } = useInput(isValidConf_pass);

  let formIsValid = false;
  if (nameIsValid && emailIsValid && passIsValid && conf_passIsValid) {
    formIsValid = true;
  }

  const submitHandler = async (event) => {
    event.preventDefault();
    if (!formIsValid) {
      return;
    }
    try {
      const { responseData, responseStatus } = await FetchSignUp({
        name: nameValue,
        email: emailValue,
        password: passValue,
        passwordConfirm: conf_passValue,
      });
      setError(null);
      setServerRes(responseData.message);
      console.log(responseStatus);
    } catch (error) {
      setError(error.message);
    }

    resetName();
    resetEmail();
    resetPass();
    resetComf_pass();
  };

  const nameClasses = nameHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];
  const emailClasses = emailHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];
  const passClasses = passHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];
  const conf_passClasses = conf_passHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];

  return (
    <PageContent title="Sign Up!">
      <h2> Please create new account</h2>

      <div className={styles.errorWindow}>
        <p>{error}</p>
        <div className={styles.errorMessage}>
          <p>{serverRes}</p>
        </div>
      </div>
      <form onSubmit={submitHandler} className={styles.form}>
        <div className={styles["control-group"]}>
          <div className={nameClasses}>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              id="name"
              value={nameValue}
              onChange={nameChangeHandler}
              onBlur={nameBlurHandler}
            />
            {nameHasError && (
              <p className={styles["error-text"]}>Please enter a valid name.</p>
            )}
          </div>

          <div className={emailClasses}>
            <label htmlFor="email">Email</label>
            <input
              type="text"
              id="email"
              value={emailValue}
              onChange={emailChangeHandler}
              onBlur={emailBlurHandler}
            />
            {emailHasError && (
              <p className={styles["error-text"]}>
                Please enter a valid email address.
              </p>
            )}
          </div>
        </div>

        <div className={passClasses}>
          <label htmlFor="password">Password</label>
          <input
            type="password" // Use password type for password fields
            id="password"
            value={passValue}
            onChange={passChangeHandler}
            onBlur={passBlurHandler}
            placeholder="Password must be 12+ chars w/ special char, number, and uppercase letter."
          />
          {passHasError && (
            <p className={styles["error-text"]}>
              Password must be 12+ chars w/ special char, number, and uppercase
              letter.
            </p>
          )}
        </div>

        <div className={conf_passClasses}>
          <label htmlFor="conf_password">Confirm Password</label>
          <input
            type="password"
            id="conf_password"
            value={conf_passValue}
            onChange={conf_passChangeHandler}
            onBlur={conf_passBlurHandler}
          />
          {conf_passHasError && (
            <p className={styles["error-text"]}>
              Password confirmation does not match the password.
            </p>
          )}
        </div>

        <div className={styles["form-actions"]}>
          <Link to={"/signin"}>Member Login </Link>

          <button disabled={!formIsValid}>Create</button>
        </div>
      </form>
    </PageContent>
  );
}

export default SignUpPage;
