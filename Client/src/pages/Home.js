import PageContent from '../components/PageContent';

function HomePage() {
  return (
    <PageContent title="Welcome!">
      <h1> Home Page</h1>
    </PageContent>
  );
}

export default HomePage;
