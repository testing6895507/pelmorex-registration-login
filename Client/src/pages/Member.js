import PageContent from "../components/PageContent";
import {getAuthToken} from '../hooks/auth'
import { useState, useEffect } from "react";


function MemberPage() {

  const token = getAuthToken()
  const [IsLogin, setIsLogin] = useState(false)

  useEffect(() => {
    if (token === null || token === undefined) {
      setIsLogin(false);
    } else {
      setIsLogin(true);
    }
  }, [token])

  return (
    <PageContent>
      <>
      
      {!IsLogin? 
      <div>
        <h1>Unauthenticated access denied.</h1>
        <h2>Please log in or create a new account to view this page</h2>

      </div> :
      <div>
        <h1>Congratulations! </h1>
        <h2>You have successfully logged in and can now access this page.</h2>
      </div>
      } 
      
      </>
    </PageContent>
  );
}

export default MemberPage;
