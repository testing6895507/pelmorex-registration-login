import { useState } from "react";
import PageContent from "../components/PageContent";
import styles from "./Sign.module.css";
import useInput from "../hooks/use-input";
import { checkEmail, checkPassword } from "../hooks/validator";
import { Link, useNavigate } from "react-router-dom";
import { FetchSignIn } from "../fetch/FetchSignin";

const isEmail = (value) => checkEmail(value);
const isValidPass = (value) => checkPassword(value);

function SignInPage() {
  const [error, setError] = useState(null);
  const [serverRes, setServerRes] = useState(null);
  const navigate = useNavigate();

  const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);
  const {
    value: passValue,
    isValid: passIsValid,
    hasError: passHasError,
    valueChangeHandler: passChangeHandler,
    inputBlurHandler: passBlurHandler,
    reset: resetPass,
  } = useInput(isValidPass);

  let formIsValid = false;
  if (emailIsValid && passIsValid) {
    formIsValid = true;
  }

  const submitHandler = async (event) => {
    event.preventDefault();
    if (!formIsValid) {
      return;
    }
    try {
      const { redirectUrl, responseData, responseStatus } = await FetchSignIn({
        email: emailValue,
        password: passValue,
      });
      setError(null);
      setServerRes(responseData.message);
      console.log(responseStatus);
      setTimeout(() => {
        navigate(redirectUrl);
      }, 3000);
    } catch (error) {
      setError(error.message);
    }

    resetEmail();
    resetPass();
  };

  const emailClasses = emailHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];
  const passClasses = passHasError
    ? `${styles["form-control"]} ${styles.invalid}`
    : styles["form-control"];

  return (
    <PageContent title="Please Login">
      <h2> Create a new account if you don't already have one </h2>
      <div className={styles.errorWindow}>
        <p>{error}</p>
        <div className={styles.errorMessage}>
          <p>{serverRes}</p>
        </div>
      </div>
      <form onSubmit={submitHandler} className={styles.form}>
        <div className={styles["control-group"]}>
          <div className={emailClasses}>
            <label htmlFor="email">Email</label>
            <input
              type="text"
              id="email"
              value={emailValue}
              onChange={emailChangeHandler}
              onBlur={emailBlurHandler}
            />
            {emailHasError && (
              <p className={styles["error-text"]}>
                Please enter a valid email address.
              </p>
            )}
          </div>
        </div>

        <div className={passClasses}>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={passValue}
            onChange={passChangeHandler}
            onBlur={passBlurHandler}
            placeholder="Password must be 12+ chars w/ special char, number, and uppercase letter."
          />
          {passHasError && (
            <p className={styles["error-text"]}>
              Password must be 12+ chars w/ special char, number, and uppercase
              letter.
            </p>
          )}
        </div>

        <div className={styles["form-actions"]}>
          <Link to={"/signup"}>Create an Account </Link>
          <button disabled={!formIsValid}>Sign In</button>
        </div>
      </form>
    </PageContent>
  );
}

export default SignInPage;
