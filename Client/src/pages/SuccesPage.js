import PageContent from "../components/PageContent";

function SuccessPage() {
  return (
    <PageContent title="Hello!">
      <h1>If you want to see the sunshine, you have to weather the storm</h1>
    </PageContent>
  );
}

export default SuccessPage;
