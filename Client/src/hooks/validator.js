import validator from "email-validator";
import passwordValidator from "password-validator";

export function checkName(name) {
  return name.trim().length >= 1;
}

export function checkEmail(email) {
  return validator.validate(email);
}

let pass = "temp";
export function checkPassword(password) {
  const schema = new passwordValidator();
  schema
  .is()
  .min(12) // Password must be at least 12 characters long
  .has()
  .uppercase() // Password must contain at least one uppercase letter
  .has()
  .lowercase() // Password must contain at least one lowercase letter
  .has()
  .digits() // Password must contain at least one digit
  .has()
  .symbols(); // Password must contain at least one special character
  
 pass = password
  return schema.validate(password);
  }

  export function checkMatch(ConfPassword) {
    return pass === ConfPassword;
  }