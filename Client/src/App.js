import { RouterProvider, createBrowserRouter } from "react-router-dom";
import ErrorPage from "./pages/Error";
import HomePage from "./pages/Home";
import RootLayout from "./pages/Root";
import { action as logoutAction } from "./pages/Logout";
import { checkAuthLoader, tokenLoader } from "./hooks/auth";
import SignInPage from "./pages/SignIn";
import SigUpPage from "./pages/SignUp";
import SuccessPage from "./pages/SuccesPage";
import MemberPage from "./pages/Member";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    id: "root",
    loader: tokenLoader,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: "member",
        element: <MemberPage/>,
        loader: checkAuthLoader, //protect the rout
      },

      {
        path: "signin",
        element: <SignInPage />,
      },
      {
        path: "signup",
        element: <SigUpPage />,
      },

      {
        path: "success",
        element: <SuccessPage />,
        loader: checkAuthLoader, //protect the rout
      },
      {
        path: "logout",
        action: logoutAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
