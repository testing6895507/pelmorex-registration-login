import { Form, NavLink } from "react-router-dom";
import classes from "./MainNavigation.module.css";
import {getAuthToken} from '../hooks/auth'
import { useState, useEffect } from "react";


function MainNavigation() {
  const token = getAuthToken()
  const [IsLogedin, setIsLogedin] = useState(false)

  useEffect(() => {
    if (token === null || token === undefined) {
      setIsLogedin(false);
    } else {
      setIsLogedin(true);
    }
  }, [token])

  return (
    <header className={classes.header}>
      <nav className={classes.headerContainer}>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Home
            </NavLink>
          </li>

          <li>
            <NavLink
              to="/member"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Member
            </NavLink>
          </li>
          
          <li>
            <NavLink
              to="/signin"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Sign In
            </NavLink>
          </li>
         {IsLogedin&& 
          <li>
            <Form action="/logout" method="post">
              <button className={classes.btn}>Log out</button>
            </Form>
          </li>
          }
        </ul>
      </nav>
    </header>
  );
}

export default MainNavigation;
