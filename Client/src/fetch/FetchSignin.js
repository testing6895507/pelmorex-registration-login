export const FetchSignIn = async (authData) => {
  try {
    const response = await fetch("http://localhost:3010/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(authData),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData.message);
    }

    const resData = await response.json();
    const token = resData.token;

    localStorage.setItem("token", token);
    const expiration = new Date();
    expiration.setHours(expiration.getHours() + 1);
    localStorage.setItem("expiration", expiration.toISOString());

    return {
      redirectUrl: "/success",
      responseData: resData,
      responseStatus: response.status,
    };
  } catch (error) {
    throw error;
  }
};
