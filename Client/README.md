## Getting Started

1. Open the `client` folder and install the necessary npm packages using the command `npm install`.
2. Ensure that the backend is running. For more information, refer to the `README.md` file in the `backend` folder.

## Account Creation

1. Navigate to the sign-up page from the home page.
2. Provide all the required details to create an account. Ensure that the password meets the following requirements:
   * Must be at least 12 characters long.
   * Must contain at least one special character, one number, and one uppercase letter.
3. Once the account is created, use the login button to log in to the server using your credentials.
4. If the login is successful, you will be redirected to a success page with the message "If you want to see the sunshine, you have to weather the storm."

## Application Testing
1.  Check the Member page:
   * When accessing the member page of the application without logging in, users will receive a  message that they do not have access to the page content.
   * After logging in with valid credentials, users will be redirected to the member page and will be able to access the page content.
 

2. To test the application, follow the steps below:
   * Try to create an account by providing the necessary details. After receiving the message "Successfully created," log out.
   * Try to send a GET request from the browser to `http://localhost:3000/success` to see the page protection without a token.
   * Use your account credentials to log in. Once the login is successful, you will receive a message about success and be redirected to the success page.

3. To test administrator functionalities, use the following login information:
   * Email: Admin@gmail.com
   * Password: Admin1235678910!@#
   * Token

Once logged in as an administrator, you will have access to additional features and privileges.

## Logging Out

1. To log out of the system and delete the session cookies from local storage, click on the logout button on the navbar.
